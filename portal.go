package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

func sayhelloName(w http.ResponseWriter, r *http.Request) {
	r.ParseForm() //Parse url parameters passed, then parse the response packet for the POST body (request body)
	// attention: If you do not call ParseForm method, the following data can not be obtained form
	fmt.Println(r.Form) // print information on server side.
	fmt.Println("path", r.URL.Path)
	fmt.Println("scheme", r.URL.Scheme)
	fmt.Println(r.Form["url_long"])
	for k, v := range r.Form {
		fmt.Println("key:", k)
		fmt.Println("val:", strings.Join(v, ""))
	}
	fmt.Fprintf(w, "Hello Mi9 Ops") // write data to response
}

func checkList(f string) bool {

	slice := []string{"apple", "pear", "banana"}

	for _, v := range slice {
		if v == f {
			return true
		}
	}
	return false
}

func login(w http.ResponseWriter, r *http.Request) {
	fmt.Println("method:", r.Method) //get request method
	if r.Method == "GET" {
		t, _ := template.ParseFiles("/home/vivek/go/src/gitlab.com/tacweb/index.html")
		t.Execute(w, nil)
	} else {
		r.ParseForm()
		if len(r.Form["username"][0]) == 0 {
			// code for empty field
			fmt.Println("Alert!: username empty")
		}

		getint, err := strconv.Atoi(r.Form.Get("age"))
		if err != nil {
			// error occurs when convert to number, it may not a number
			fmt.Println("Alert!: age not valid")
		}

		// check range of number
		if getint > 100 {
			// too big
			fmt.Println("Alert!: age too large")
		}

		if m, _ := regexp.MatchString(`^([\w\.\_]{2,10})@(\w{1,}).([a-z]{2,4})$`, r.Form.Get("email")); !m {
			fmt.Println("no")
			fmt.Println("Alert!: invalid email")
		} else {
			fmt.Println("yes")
			fmt.Println("Alert!: correct email")
		}

		res := checkList(r.Form.Get("fruit"))

		if res == true {
			fmt.Println("Alert!: valid fruit")

		} else {
			fmt.Println("Alert!: invalid fruit")

		}

		// logic part of log in
		fmt.Println("username:", r.Form["username"])
		fmt.Println("password:", r.Form["password"])
		fmt.Println("fruit:", r.Form["fruit"])
		fmt.Println("email:", r.Form["email"])
		fmt.Println("age:", r.Form["age"])
	}
}

func serveSingle(pattern string, filename string) {
	http.HandleFunc(pattern, func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, filename)
	})
}

func main() {
	http.HandleFunc("/", sayhelloName) // setting router rule
	http.HandleFunc("/login", login)

	serveSingle("/sitemap.xml", "./sitemap.xml")
	serveSingle("/favicon2.ico", "./favicon2.ico")
	serveSingle("/robots.txt", "./robots.txt")
	serveSingle("/signin.css", "./signin.css")
	serveSingle("/css/bootstrap.min.css", "./css/bootstrap.min.css")
	//    serveSingle("/", "")

	err := http.ListenAndServe(":5000", nil) // setting listening port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
